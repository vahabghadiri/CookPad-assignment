package com.vahabghadiri.presentation.feature.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ProgressBar
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.snackbar.Snackbar
import com.vahabghadiri.presentation.R
import com.vahabghadiri.presentation.base.BaseFragment
import com.vahabghadiri.presentation.databinding.FragmentDetailBinding
import com.vahabghadiri.presentation.databinding.FragmentSearchBinding
import com.vahabghadiri.presentation.feature.search.adapter.AdapterCallback
import com.vahabghadiri.presentation.feature.search.adapter.SearchListRvAdapter
import com.vahabghadiri.presentation.feature.search.model.RecipeListItem
import com.vahabghadiri.presentation.utils.extension.createViewModel
import com.vahabghadiri.presentation.utils.extension.isExpanded
import com.vahabghadiri.presentation.utils.extension.observeNullSafe
import com.vahabghadiri.presentation.utils.extension.onAfterTextChanged
import com.vahabghadiri.presentation.utils.extension.setVisible
import com.vahabghadiri.presentation.utils.view.PaginationScrollListener

class SearchFragment : BaseFragment(), AdapterCallback {

    private lateinit var viewBinding: FragmentSearchBinding
    private lateinit var viewModel: SearchViewModel

    private var failureView: Snackbar? = null
    private var recyclerAdapter: SearchListRvAdapter? = null
    private var isListInLoading = false

    /**
     * endless scroll listener to have
     * end event and load on recyclerView
     * @see setupPagination
     */
    private var paginationScrollListener: PaginationScrollListener? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewBinding = FragmentSearchBinding.inflate(inflater)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews(view)
        restoreViewsState(savedInstanceState)
        setupViewModel()
    }

    private fun restoreViewsState(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            return
        }
        viewBinding.appBar.setExpanded(savedInstanceState.getBoolean(APPBAR_EXPAND_STATE_KEY))
    }

    private fun setupViews(view: View) {

        // init the recyclerView
        with(viewBinding.rvList) {
            recyclerAdapter = SearchListRvAdapter(
                mutableListOf(),
                this@SearchFragment
            )
            GridLayoutManager(requireContext(), getProperSpanCount()).let {
                this.layoutManager = it
                setupPagination(it, this)
            }
            this.adapter = recyclerAdapter
        }

        // init searchView
        view.findViewById<EditText>(R.id.edtSearch).apply {
            viewBinding.searchBox.edtSearch.onAfterTextChanged {
                viewModel.onUserChangedSearchString(it)
            }
        }
    }

    private fun getProperSpanCount(): Int {
        return requireContext().resources.getInteger(R.integer.list_span_count)
    }

    private fun setupViewModel() {
        viewModel = createViewModel(viewModelFactory) {
            updateListLiveData.observeNullSafe(viewLifecycleOwner) {
                onSearchResultListUpdated(it)
            }
            navigateToDetailsLiveData.observeNullSafe(viewLifecycleOwner) { recipeId ->
                navigateToDetails(recipeId)
            }
            progressVisibilityLiveData.observeNullSafe(viewLifecycleOwner) { (isVisible, shouldShowCenterProgress) ->
                onProgressVisibilityChange(isVisible, shouldShowCenterProgress)
            }
            emptyViewVisibilityLiveData.observeNullSafe(viewLifecycleOwner) { isVisible ->
                onEmptyViewVisibilityChange(isVisible)
            }
            failureEventLiveData.observe(viewLifecycleOwner) { failureMessage ->
                handleFailureViews(failureMessage)
            }
        }
    }

    /**
     * will setup pagination for recycler view
     * by adding custom scroll listener and will notify
     * view model when user reaches the list end.
     */
    private fun setupPagination(it: LinearLayoutManager, recyclerView: RecyclerView) {
        paginationScrollListener = object : PaginationScrollListener(it) {

            override fun isLoading(): Boolean = isListInLoading

            override fun loadMoreItems() {
                viewModel.onPageLoadMoreItem()
            }
        }.apply {
            recyclerView.addOnScrollListener(this)
        }
    }

    private fun onSearchResultListUpdated(newList: List<RecipeListItem>) {
        recyclerAdapter?.updateData(newList)
    }

    override fun onItemClick(adapterPosition: Int) {
        recyclerAdapter?.getItemWithPosition(adapterPosition)?.let {
            viewModel.onSearchResultItemClicked(it)
        }
    }

    /**
     * this page has persisted snack bar with retry as error view
     * so when failure message got null this snackbar will be dismissed.
     */
    private fun handleFailureViews(failureMessage: String?) {
        if (failureMessage.isNullOrBlank()) {
            failureView?.dismiss()
        } else {
            showErrorSnackBar(failureMessage)
        }
    }

    private fun showErrorSnackBar(message: String) {
        failureView?.dismiss()
        failureView = Snackbar.make(viewBinding.root, message, Snackbar.LENGTH_INDEFINITE).apply {
            setAction(getString(R.string.retry)) {
                viewModel.onRetryClick()
            }
        }
        failureView?.show()
    }

    private fun onProgressVisibilityChange(isVisible: Boolean, shouldShowCenterProgress: Boolean) {
        isListInLoading = isVisible
        if (shouldShowCenterProgress) {
            viewBinding.pgLoading.setVisible(false)
            viewBinding.pgFullScreen.setVisible(isVisible)
        } else {
            viewBinding.pgFullScreen.setVisible(false)
            viewBinding.pgLoading.setVisible(isVisible)
        }
    }

    private fun onEmptyViewVisibilityChange(isVisible: Boolean) {
        viewBinding.emptyLayout.tvEmpty.setVisible(isVisible)
    }

    private fun navigateToDetails(recipeId: String) {
        val direction = SearchFragmentDirections.actionSearchFragmentToDetailsFragment(recipeId)
        findNavController().navigate(direction)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        if (this::viewBinding.isInitialized){
            outState.putBoolean(APPBAR_EXPAND_STATE_KEY, viewBinding.appBar.isExpanded())
        }
        super.onSaveInstanceState(outState)
    }

    override fun onDestroy() {
        // to prevent memory leaks by listeners and adapter
        recyclerAdapter = null
        super.onDestroy()
    }

    companion object {

        private const val APPBAR_EXPAND_STATE_KEY = "appbarExpandStateKey"
    }
}
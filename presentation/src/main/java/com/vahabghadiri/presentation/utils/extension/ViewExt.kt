package com.vahabghadiri.presentation.utils.extension

import android.os.Build
import android.text.Editable
import android.text.Html
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.TextView
import com.google.android.material.appbar.AppBarLayout

/**
 * extension on Edit text which will add text watcher listener
 * and it only have text change callback
 */
fun EditText.onAfterTextChanged(f: (String) -> Unit) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {
            p0?.let { f.invoke(it.toString()) }
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }
    })
}

fun AppBarLayout.isExpanded() = height - bottom == 0

fun View.setVisible(isVisible: Boolean) {
    visibility = if (isVisible) {
        View.VISIBLE
    } else {
        View.GONE
    }
}

fun TextView.setHtmlText(htmlString: String) {
    text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        Html.fromHtml(
            htmlString,
            Html.FROM_HTML_MODE_COMPACT
        )
    } else {
        Html.fromHtml(htmlString)
    }
}
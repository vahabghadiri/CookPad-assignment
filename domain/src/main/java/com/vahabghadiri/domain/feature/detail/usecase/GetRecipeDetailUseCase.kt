package com.vahabghadiri.domain.feature.detail.usecase

import com.vahabghadiri.domain.feature.detail.model.RecipeDetailDomainModel
import com.vahabghadiri.domain.feature.detail.repository.RecipeDetailRepository
import com.vahabghadiri.domain.model.Result
import com.vahabghadiri.domain.model.usecase.UseCase
import javax.inject.Inject

/**
 * UseCase to use recipe id to get details as result.
 * @see UseCase
 * @see Result
 */
class GetRecipeDetailUseCase @Inject constructor(
    private val detailRepository: RecipeDetailRepository
) : UseCase<RecipeDetailDomainModel, RecipeDetailParam> {

    override suspend fun execute(params: RecipeDetailParam): Result<RecipeDetailDomainModel> {
        return detailRepository.getRecipeDetail(params.id)
    }
}

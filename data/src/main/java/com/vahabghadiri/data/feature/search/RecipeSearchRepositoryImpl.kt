package com.vahabghadiri.data.feature.search

import com.vahabghadiri.data.feature.search.remote.RecipeSearchRemoteDataSource
import com.vahabghadiri.data.utils.ApiResult
import com.vahabghadiri.domain.feature.search.model.RecipeSearchResultDomainModel
import com.vahabghadiri.domain.feature.search.repository.RecipeSearchRepository
import com.vahabghadiri.domain.model.Result
import javax.inject.Inject
import javax.inject.Singleton

/**
 * implementation for recipe search repository abstraction in
 * domain layer.
 * @see RecipeSearchRepository
 */
@Singleton
class RecipeSearchRepositoryImpl @Inject constructor(
    private val recipeSearchRemoteDataSource: RecipeSearchRemoteDataSource
) : RecipeSearchRepository {

    override suspend fun searchRecipes(
        query: String,
        offset: Int
    ): Result<RecipeSearchResultDomainModel> =
        when (val apiResult = recipeSearchRemoteDataSource.searchRecipes(query, offset)) {
            is ApiResult.Success -> {
                Result.Success(apiResult.value.toDomainModel())
            }
            is ApiResult.Failure -> {
                Result.Failure(apiResult.error.message ?: UNKNOWN_API_EXCEPTION)
            }
        }

    companion object {

        private const val UNKNOWN_API_EXCEPTION = "unknown api exception"
    }
}
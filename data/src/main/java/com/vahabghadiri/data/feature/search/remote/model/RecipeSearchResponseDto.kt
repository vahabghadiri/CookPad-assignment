package com.vahabghadiri.data.feature.search.remote.model

import com.google.gson.annotations.SerializedName

data class RecipeSearchResponseDto(
    @SerializedName("results") val results: List<RecipeDto>,
    @SerializedName("offset") val offset: Int,
    @SerializedName("number") val number: Int,
    @SerializedName("totalResults") val totalResults: Int,
)

data class RecipeDto(
    @SerializedName("id") val id: String,
    @SerializedName("title") val title: String,
    @SerializedName("image") val image: String
)
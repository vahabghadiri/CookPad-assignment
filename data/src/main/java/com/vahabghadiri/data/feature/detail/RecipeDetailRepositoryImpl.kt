package com.vahabghadiri.data.feature.detail

import com.vahabghadiri.data.feature.detail.local.RecipeDetailLocalDataSource
import com.vahabghadiri.data.feature.detail.remote.RecipeDetailRemoteDataSource
import com.vahabghadiri.data.utils.ApiResult
import com.vahabghadiri.domain.feature.detail.model.RecipeDetailDomainModel
import com.vahabghadiri.domain.feature.detail.repository.RecipeDetailRepository
import com.vahabghadiri.domain.model.Result
import javax.inject.Inject
import javax.inject.Singleton

/**
 * implementation for recipe search repository abstraction in
 * domain layer.
 * @see RecipeDetailRepository
 */
@Singleton
class RecipeDetailRepositoryImpl @Inject constructor(
    private val recipeDetailRemoteDataSource: RecipeDetailRemoteDataSource,
    private val recipeDetailLocalDataSource: RecipeDetailLocalDataSource
) : RecipeDetailRepository {

    override suspend fun getRecipeDetail(recipeId: String): Result<RecipeDetailDomainModel> {
        val persistedDetails = recipeDetailLocalDataSource.getRecipe(recipeId)
        if (persistedDetails != null) {
            return Result.Success(persistedDetails.toDomainModel())
        }
        return when (val apiResult = recipeDetailRemoteDataSource.getRecipeDetail(recipeId)) {
            is ApiResult.Success -> {
                recipeDetailLocalDataSource.saveRecipe(apiResult.value)
                Result.Success(apiResult.value.toDomainModel())
            }
            is ApiResult.Failure -> {
                Result.Failure(apiResult.error.message ?: UNKNOWN_API_EXCEPTION)
            }
        }
    }

    companion object {

        private const val UNKNOWN_API_EXCEPTION = "unknown api exception"
    }
}
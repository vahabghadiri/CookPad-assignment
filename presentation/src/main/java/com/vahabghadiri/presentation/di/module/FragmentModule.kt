package com.vahabghadiri.presentation.di.module

import com.vahabghadiri.presentation.feature.detail.DetailFragment
import com.vahabghadiri.presentation.feature.search.SearchFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * di module for injection in fragments so they will contribute for
 * android injection.
 */
@Module
abstract class FragmentModule {

    @ContributesAndroidInjector
    abstract fun searchFragment(): SearchFragment

    @ContributesAndroidInjector
    abstract fun detailsFragment(): DetailFragment
}
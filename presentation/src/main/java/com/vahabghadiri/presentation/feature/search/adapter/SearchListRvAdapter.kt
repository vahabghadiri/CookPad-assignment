package com.vahabghadiri.presentation.feature.search.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.vahabghadiri.presentation.R
import com.vahabghadiri.presentation.feature.search.model.RecipeListItem

/**
 * recycler view adapter for search recycle view
 * this uses DiffUtil to handle changes
 * because we have search functionality.
 */
class SearchListRvAdapter(
    private val data: MutableList<RecipeListItem>,
    private val adapterCallback: AdapterCallback
) : RecyclerView.Adapter<SearchResultItemViewHolder>() {

    fun updateData(newList: List<RecipeListItem>) {
        val diffCallback = SearchResultDiffCallback(data, newList)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        data.clear()
        data.addAll(newList)
        diffResult.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchResultItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_list_recipe, parent, false)
        return SearchResultItemViewHolder(view, adapterCallback)
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: SearchResultItemViewHolder, position: Int) {
        if (position != RecyclerView.NO_POSITION) {
            holder.bind(data[position])
        }
    }

    /**
     * will get certain item in adapter with position
     */
    fun getItemWithPosition(adapterPosition: Int): RecipeListItem? {
        return if (adapterPosition != RecyclerView.NO_POSITION) {
            data[adapterPosition]
        } else {
            null
        }
    }
}
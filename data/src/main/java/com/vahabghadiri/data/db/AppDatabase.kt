package com.vahabghadiri.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.vahabghadiri.data.feature.detail.local.RecipeDetailDao
import com.vahabghadiri.data.feature.detail.local.model.RecipeDetailLocalEntity

@Database(entities = [RecipeDetailLocalEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun recipeDetailDao(): RecipeDetailDao
}
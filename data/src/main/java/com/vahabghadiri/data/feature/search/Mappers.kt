package com.vahabghadiri.data.feature.search

import com.vahabghadiri.data.feature.search.model.RecipeDataModel
import com.vahabghadiri.data.feature.search.model.RecipeSearchDataModel
import com.vahabghadiri.data.feature.search.remote.model.RecipeDto
import com.vahabghadiri.data.feature.search.remote.model.RecipeSearchResponseDto
import com.vahabghadiri.domain.feature.search.model.RecipeDomainModel
import com.vahabghadiri.domain.feature.search.model.RecipeSearchResultDomainModel

/**
 * this file is responsible for data layer
 * mapping methods.
 * it could be separated to data source layer and repository layer files.
 */
fun RecipeSearchResponseDto.toDataModel() = RecipeSearchDataModel(
    results = results.map { it.toRecipeDataModel() },
    offset, number, totalResults
)

fun RecipeSearchDataModel.toDomainModel() = RecipeSearchResultDomainModel(
    results = results.map { it.toRecipeDomainModel() },
    offset, number, totalResults
)

fun RecipeDto.toRecipeDataModel() = RecipeDataModel(
    id, title, image
)

fun RecipeDataModel.toRecipeDomainModel() = RecipeDomainModel(
    id, title, image
)
package com.vahabghadiri.presentation.feature.search.adapter

interface AdapterCallback {
    fun onItemClick(adapterPosition: Int)
}
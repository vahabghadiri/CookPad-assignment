package com.vahabghadiri.presentation.base

import androidx.lifecycle.ViewModelProvider
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

/**
 * @author vahab ghadiri
 * base dagger implementations for activity
 */
abstract class BaseActivity : DaggerAppCompatActivity() {

    /**
     * will provide factory for viewModel creation
     */
    @Inject
    protected lateinit var viewModelFactory: ViewModelProvider.Factory
}
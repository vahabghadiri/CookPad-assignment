package com.vahabghadiri.presentation.feature.search

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.argumentCaptor
import com.nhaarman.mockitokotlin2.atLeastOnce
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.stub
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.vahabghadiri.core.utils.GlobalDispatcher
import com.vahabghadiri.core.utils.testCase
import com.vahabghadiri.domain.feature.search.model.RecipeDomainModel
import com.vahabghadiri.domain.feature.search.model.RecipeSearchResultDomainModel
import com.vahabghadiri.domain.feature.search.usecase.SearchRecipeParam
import com.vahabghadiri.domain.feature.search.usecase.SearchRecipesUseCase
import com.vahabghadiri.domain.model.Result
import com.vahabghadiri.presentation.feature.search.model.RecipeListItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentCaptor
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
internal class SearchViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var viewModel: SearchViewModel
    private lateinit var globalDispatcher: GlobalDispatcher
    private val searchRecipesUseCase: SearchRecipesUseCase = mock()
    private val viewModelDebounceDelayMs = 700L

    @Before
    fun setup() {
        globalDispatcher = GlobalDispatcher(
            main = Dispatchers.Unconfined,
            io = Dispatchers.Unconfined,
            default = Dispatchers.Unconfined
        )
        viewModel = SearchViewModel(
            searchRecipesUseCase,
            globalDispatcher
        )
    }

    @Test
    fun `should clear user list if query is empty`() = testCase {
        val observer = mock<Observer<List<RecipeListItem>>>()
        val argumentCaptor = argumentCaptor<List<RecipeListItem>>()
        val fakeQueryString = ""
        given {
            viewModel.updateListLiveData.observeForever(observer)
        }
        whenever(viewModel) {
            onUserChangedSearchString(fakeQueryString)
        }
        then {
            verify(observer, times(1)).onChanged(argumentCaptor.capture())
            val observerChanges = argumentCaptor.allValues.first()
            assert(observerChanges.isEmpty())
        }
    }

    @Test
    fun `should not start search if query length is less than minimum character`() = testCase {

        val fakeQueryString = "ab"
        val param = SearchRecipeParam(fakeQueryString)

        whenever(viewModel) {
            runBlocking {
                onUserChangedSearchString(fakeQueryString)
                delay(viewModelDebounceDelayMs)
            }
        }
        then {
            runBlocking {
                verify(searchRecipesUseCase, times(0)).execute(param)
            }
        }
    }

    @Test
    fun `should reset search on user typing`() = testCase {
        val fakeQueryString = "abc"

        whenever(viewModel) {
            onUserChangedSearchString(fakeQueryString)
        }
        then {
            runBlocking {
                verify(searchRecipesUseCase, times(1)).refreshToFirstPage()
            }
        }
    }

    @Test
    fun `should not start search if user is typing`() = testCase {
        val fakeQueryString = "abcd"
        val param = SearchRecipeParam(fakeQueryString)

        whenever(viewModel) {
            runBlocking {
                onUserChangedSearchString(fakeQueryString)
            }
        }
        then {
            runBlocking {
                verify(searchRecipesUseCase, times(0)).execute(param)
            }
        }
    }

    @Test
    fun `should navigate to details page when user click on item`() = testCase {
        val observer = mock<Observer<String>>()
        val argumentCaptor = argumentCaptor<String>()
        val listItem = RecipeListItem("id", "title", "null")
        given {
            viewModel.navigateToDetailsLiveData.observeForever(observer)
        }
        whenever(viewModel) {
            onSearchResultItemClicked(listItem)
        }
        then {
            verify(observer, times(1)).onChanged(argumentCaptor.capture())
        }
    }

    @Test
    fun `should start to search on retry when there was a search before`() = testCase {
        val fakeQueryString = "abcd"
        val param = SearchRecipeParam(fakeQueryString)

        given {
            searchRecipesUseCase.stub {
                onBlocking {
                    execute(param)
                } doReturn Result.Failure("exception message")
            }
        }
        whenever(viewModel) {
            runBlocking {
                onUserChangedSearchString(fakeQueryString)
                delay(viewModelDebounceDelayMs)
                onRetryClick()
                delay(viewModelDebounceDelayMs)
            }
        }
        then {
            runBlocking {
                verify(
                    searchRecipesUseCase,
                    times(2)
                ).execute(param)
            }
        }
    }

    @Test
    fun `should start to search on paging`() = testCase {
        val fakeQueryString = "abcd"
        val param = SearchRecipeParam(fakeQueryString)
        val fakePageResult = RecipeSearchResultDomainModel(emptyList(), 0, 0, 0)
        given {
            searchRecipesUseCase.stub {
                onBlocking {
                    execute(param)
                } doReturn Result.Success(fakePageResult)
            }
        }
        whenever(viewModel) {
            runBlocking {
                onUserChangedSearchString(fakeQueryString)
                delay(viewModelDebounceDelayMs)
                onPageLoadMoreItem()
                delay(viewModelDebounceDelayMs)
            }
        }
        then {
            runBlocking {
                verify(
                    searchRecipesUseCase,
                    times(2)
                ).execute(param)
            }
        }
    }

    @Test
    fun `should update list data on api succeed`() = testCase {
        val observer = mock<Observer<List<RecipeListItem>>>()
        val argumentCaptor = argumentCaptor<List<RecipeListItem>>()

        val fakeQueryString = "abvcd"
        val param = SearchRecipeParam(fakeQueryString)
        val fakeData = List(5) {
            RecipeDomainModel(it.toString(), "null", "null")
        }
        val fakePageResult = RecipeSearchResultDomainModel(fakeData, 0, 0, 0)
        given {
            searchRecipesUseCase.stub {
                onBlocking {
                    execute(param)
                } doReturn Result.Success(fakePageResult)
            }
            viewModel.updateListLiveData.observeForever(observer)
        }
        whenever(viewModel) {
            runBlocking {
                onUserChangedSearchString(fakeQueryString)
                delay(viewModelDebounceDelayMs)
                onPageLoadMoreItem()
                delay(viewModelDebounceDelayMs)
            }
        }
        then {
            verify(observer, atLeastOnce()).onChanged(argumentCaptor.capture())
            assert(argumentCaptor.allValues.last().size == 10)
        }
    }

    @Test
    fun `should show error on api failure`() = testCase {
        val observer = mock<Observer<String?>>()
        val argumentCaptor = argumentCaptor<String>()

        val fakeQueryString = "abcd"
        val param = SearchRecipeParam(fakeQueryString)
        val fakePageResult = Result.Failure("fake error message")
        given {
            searchRecipesUseCase.stub {
                onBlocking {
                    execute(param)
                } doReturn fakePageResult
            }
            viewModel.failureEventLiveData.observeForever(observer)
        }
        whenever(viewModel) {
            runBlocking {
                onUserChangedSearchString(fakeQueryString)
                delay(viewModelDebounceDelayMs)
            }
        }
        then {
            verify(observer, atLeastOnce()).onChanged(argumentCaptor.capture())
        }
    }

    @Test
    fun `should show empty view if data is empty`() = testCase {
        val observer = mock<Observer<Boolean>>()
        val argumentCaptor = argumentCaptor<Boolean>()

        val fakeQueryString = "abcd"
        val param = SearchRecipeParam(fakeQueryString)
        val fakePageResult = Result.Success(
            RecipeSearchResultDomainModel(emptyList(), 0, 0, 0)
        )
        given {
            searchRecipesUseCase.stub {
                onBlocking {
                    execute(param)
                } doReturn fakePageResult
            }
            viewModel.emptyViewVisibilityLiveData.observeForever(observer)
        }
        whenever(viewModel) {
            runBlocking {
                onUserChangedSearchString(fakeQueryString)
                delay(viewModelDebounceDelayMs)
            }
        }
        then {
            verify(observer, atLeastOnce()).onChanged(argumentCaptor.capture())
            assert(argumentCaptor.allValues.last())
        }
    }

    @Test
    fun `should hide error view on search start`() = testCase {
        val observer = mock<Observer<String?>>()
        val argumentCaptor = ArgumentCaptor.forClass(String::class.java)

        val fakeQueryString = "abc"

        given {
            viewModel.failureEventLiveData.observeForever(observer)
        }
        whenever(viewModel) {
            onUserChangedSearchString(fakeQueryString)
        }
        then {
            verify(observer, atLeastOnce()).onChanged(argumentCaptor.capture())
            assert(argumentCaptor.allValues.first() == null)
        }
    }
}
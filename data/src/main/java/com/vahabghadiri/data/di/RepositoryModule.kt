package com.vahabghadiri.data.di

import com.vahabghadiri.data.feature.detail.RecipeDetailRepositoryImpl
import com.vahabghadiri.data.feature.search.RecipeSearchRepositoryImpl
import com.vahabghadiri.domain.feature.detail.repository.RecipeDetailRepository
import com.vahabghadiri.domain.feature.search.repository.RecipeSearchRepository
import dagger.Binds
import dagger.Module

/**
 * this module will bind all domain layer abstractions to data
 * layer implementations.
 */
@Module
abstract class RepositoryModule {

    @Binds
    abstract fun bindSearchRepository(
        searchRepositoryImpl: RecipeSearchRepositoryImpl
    ): RecipeSearchRepository

    @Binds
    abstract fun bindDetailRepository(
        detailRepositoryImpl: RecipeDetailRepositoryImpl
    ): RecipeDetailRepository
}
package com.vahabghadiri.data.feature.search.remote

import com.vahabghadiri.data.feature.search.remote.model.RecipeSearchResponseDto
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Query

interface RecipeSearchApiService {

    @GET("recipes/complexSearch")
    fun getSearchResult(
        @Query("query") searchQuery: String,
        @Query("number") pageCount: Int,
        @Query("offset") offset: Int
    ): Call<RecipeSearchResponseDto>
}
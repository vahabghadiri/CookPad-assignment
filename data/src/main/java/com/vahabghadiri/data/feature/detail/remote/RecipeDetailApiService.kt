package com.vahabghadiri.data.feature.detail.remote

import com.vahabghadiri.data.feature.detail.remote.model.RecipeDetailResponseDto
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface RecipeDetailApiService {

    @GET("recipes/{id}/information")
    fun getRecipeDetails(
        @Path("id") recipeId: String
    ): Call<RecipeDetailResponseDto>
}
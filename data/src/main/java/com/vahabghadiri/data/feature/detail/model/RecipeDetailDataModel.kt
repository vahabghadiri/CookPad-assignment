package com.vahabghadiri.data.feature.detail.model

/**
 * model in repository layer
 */
data class RecipeDetailDataModel(
    val id: String,
    val title: String,
    val image: String,
    val instructions: String
)
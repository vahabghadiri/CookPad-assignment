package com.vahabghadiri.domain.feature.search.repository

import com.vahabghadiri.domain.feature.search.model.RecipeSearchResultDomainModel
import com.vahabghadiri.domain.model.Result

/**
 * search repository abstraction in domain layer
 * which is implemented by repository in data layer
 * app uses repository pattern
 */
interface RecipeSearchRepository {

    suspend fun searchRecipes(
        query: String,
        offset: Int
    ): Result<RecipeSearchResultDomainModel>
}
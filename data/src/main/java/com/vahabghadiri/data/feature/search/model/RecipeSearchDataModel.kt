package com.vahabghadiri.data.feature.search.model

/**
 * model in repository layer
 */
data class RecipeSearchDataModel(
    val results: List<RecipeDataModel>,
    val offset: Int,
    val number: Int,
    val totalResults: Int,
)

data class RecipeDataModel(
    val id: String,
    val title: String,
    val image: String
)
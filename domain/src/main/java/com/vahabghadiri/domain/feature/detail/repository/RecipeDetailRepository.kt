package com.vahabghadiri.domain.feature.detail.repository

import com.vahabghadiri.domain.feature.detail.model.RecipeDetailDomainModel
import com.vahabghadiri.domain.model.Result

/**
 * details repository abstraction in domain layer
 * which is implemented by repository in data layer
 * app uses repository pattern
 */
interface RecipeDetailRepository {

    suspend fun getRecipeDetail(
        recipeId: String
    ): Result<RecipeDetailDomainModel>
}
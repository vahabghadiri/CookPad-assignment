package com.vahabghadiri.presentation.feature.search.adapter

import androidx.recyclerview.widget.DiffUtil
import com.vahabghadiri.presentation.feature.search.model.RecipeListItem

/**
 * diff util callback for search
 * it will uses name as unique id
 */
class SearchResultDiffCallback(
    private val old: List<RecipeListItem>,
    private val new: List<RecipeListItem>
) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return old[oldItemPosition].id == new[newItemPosition].id
    }

    override fun getOldListSize(): Int = old.size

    override fun getNewListSize(): Int = new.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return old[oldItemPosition].title == new[newItemPosition].title &&
                return old[oldItemPosition].image == new[newItemPosition].image
    }
}
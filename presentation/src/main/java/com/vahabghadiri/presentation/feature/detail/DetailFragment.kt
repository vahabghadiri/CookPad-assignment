package com.vahabghadiri.presentation.feature.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.google.android.material.snackbar.Snackbar
import com.vahabghadiri.presentation.R
import com.vahabghadiri.presentation.base.BaseFragment
import com.vahabghadiri.presentation.databinding.FragmentDetailBinding
import com.vahabghadiri.presentation.feature.detail.model.RecipeDetailViewItem
import com.vahabghadiri.presentation.utils.ImageLoader
import com.vahabghadiri.presentation.utils.extension.createViewModel
import com.vahabghadiri.presentation.utils.extension.observeNullSafe
import com.vahabghadiri.presentation.utils.extension.setHtmlText
import com.vahabghadiri.presentation.utils.extension.setVisible

class DetailFragment : BaseFragment() {

    private lateinit var viewModel: DetailViewModel
    private lateinit var viewBinding: FragmentDetailBinding
    private var failureView: Snackbar? = null
    private val args: DetailFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewBinding = FragmentDetailBinding.inflate(inflater)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupViewModel()
        viewModel.onDetailViewCreated(args.receipId, savedInstanceState)
    }

    private fun setupViewModel() {
        viewModel = createViewModel(viewModelFactory) {
            progressVisibilityLiveData.observeNullSafe(viewLifecycleOwner) { isVisible ->
                onProgressVisibilityChange(isVisible)
            }
            failureEventLiveData.observe(viewLifecycleOwner) { failureMessage ->
                handleFailureViews(failureMessage)
            }
            updateViewLiveData.observeNullSafe(viewLifecycleOwner) {
                updateViewItem(it)
            }
        }
    }

    private fun handleFailureViews(failureMessage: String?) {
        if (failureMessage.isNullOrBlank()) {
            failureView?.dismiss()
        } else {
            showErrorSnackBar(failureMessage)
        }
    }

    private fun showErrorSnackBar(message: String) {
        failureView?.dismiss()
        failureView = Snackbar.make(viewBinding.root, message, Snackbar.LENGTH_INDEFINITE).apply {
            setAction(getString(R.string.retry)) {
                viewModel.onRetryClick(args.receipId)
            }
        }
        failureView?.show()
    }

    private fun updateViewItem(item: RecipeDetailViewItem) {
        with(viewBinding) {
            ImageLoader.load(imageViewDetails, item.image)
            body.title.text = item.title
            body.instruction.setHtmlText(item.instructions)
        }
    }

    private fun onProgressVisibilityChange(isVisible: Boolean) {
        viewBinding.progress.setVisible(isVisible)
    }
}
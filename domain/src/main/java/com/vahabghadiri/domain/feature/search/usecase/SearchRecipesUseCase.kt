package com.vahabghadiri.domain.feature.search.usecase

import com.vahabghadiri.domain.feature.search.model.RecipeSearchResultDomainModel
import com.vahabghadiri.domain.feature.search.repository.RecipeSearchRepository
import com.vahabghadiri.domain.model.Result
import com.vahabghadiri.domain.model.usecase.UseCase
import javax.inject.Inject

/**
 * UseCase to use query and search for recipe objects and return result.
 * class will use SearchRecipeParam as an input param and will
 * return Result model of RecipesSearchResultDomainModel
 * @see UseCase
 * @see Result
 */
class SearchRecipesUseCase @Inject constructor(
    private val searchRepository: RecipeSearchRepository
) : UseCase<RecipeSearchResultDomainModel, SearchRecipeParam> {

    private var pageOffset = DEFAULT_PAGE_OFFSET
    private var totalCount = DEFAULT_TOTAL_COUNT

    override suspend fun execute(
        params: SearchRecipeParam
    ): Result<RecipeSearchResultDomainModel> {

        val result = searchRepository.searchRecipes(params.searchQuery, pageOffset)
        if (result is Result.Success) {
            updatePageData(result)
        }
        return result
    }

    fun isReachedTheLastPage(): Boolean = if (totalCount == DEFAULT_TOTAL_COUNT) {
        false
    } else {
        pageOffset >= totalCount
    }

    private fun updatePageData(result: Result.Success<RecipeSearchResultDomainModel>) {
        totalCount = result.value.totalResults
        pageOffset += result.value.number
    }

    fun refreshToFirstPage() {
        pageOffset = DEFAULT_PAGE_OFFSET
        totalCount = DEFAULT_TOTAL_COUNT
    }

    companion object {

        private const val DEFAULT_PAGE_OFFSET = 0
        private const val DEFAULT_TOTAL_COUNT = -1
    }
}

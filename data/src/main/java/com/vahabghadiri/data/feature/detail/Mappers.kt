package com.vahabghadiri.data.feature.detail

import com.vahabghadiri.data.feature.detail.model.RecipeDetailDataModel
import com.vahabghadiri.data.feature.detail.remote.model.RecipeDetailResponseDto
import com.vahabghadiri.domain.feature.detail.model.RecipeDetailDomainModel

fun RecipeDetailResponseDto.toDataModel() = RecipeDetailDataModel(
    id, title, image, instructions
)

fun RecipeDetailDataModel.toDomainModel() = RecipeDetailDomainModel(
    id, title, image, instructions
)
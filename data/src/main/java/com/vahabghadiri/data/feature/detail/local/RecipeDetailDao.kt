package com.vahabghadiri.data.feature.detail.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.vahabghadiri.data.feature.detail.local.model.RecipeDetailLocalEntity

@Dao
interface RecipeDetailDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveObject(recipeDetailLocalEntity: RecipeDetailLocalEntity)

    @Query("select * from recipe_detail where id=:id")
    suspend fun getObject(id: String): RecipeDetailLocalEntity?
}
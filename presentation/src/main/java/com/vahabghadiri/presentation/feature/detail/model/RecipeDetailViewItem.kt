package com.vahabghadiri.presentation.feature.detail.model

data class RecipeDetailViewItem(
    val title: String,
    val image: String,
    val instructions: String
)
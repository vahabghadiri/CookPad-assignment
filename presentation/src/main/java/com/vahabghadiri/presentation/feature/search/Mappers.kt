package com.vahabghadiri.presentation.feature.search

import com.vahabghadiri.domain.feature.search.model.RecipeDomainModel
import com.vahabghadiri.presentation.feature.search.model.RecipeListItem

/**
 * mapper for ui items.
 * ui items could have view parameters like isExpanding or isVisible
 * so app needs these mappers
 */
fun RecipeDomainModel.toListItem() = RecipeListItem(
    id, title, image
)
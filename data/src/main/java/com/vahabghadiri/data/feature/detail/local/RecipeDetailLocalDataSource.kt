package com.vahabghadiri.data.feature.detail.local

import com.vahabghadiri.core.utils.GlobalDispatcher
import com.vahabghadiri.data.feature.detail.model.RecipeDetailDataModel
import kotlinx.coroutines.withContext
import javax.inject.Inject

class RecipeDetailLocalDataSource @Inject constructor(
    private val recipeDetailDao: RecipeDetailDao,
    private val globalDispatcher: GlobalDispatcher
) {

    suspend fun saveRecipe(
        recipeDetailDataModel: RecipeDetailDataModel
    ) = withContext(globalDispatcher.io) {
        recipeDetailDao.saveObject(recipeDetailDataModel.toLocalEntity())
    }

    suspend fun getRecipe(
        id: String
    ): RecipeDetailDataModel? = withContext(globalDispatcher.io) {
        recipeDetailDao.getObject(id)?.toDataModel()
    }
}
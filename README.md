
## About App
This is a assignment for ----.

## IMPORTANT
Since i couldn't find any unlimited recipe app and I insisted to have a recipe app, therefore
used API-token has 150 requests per day quota.
this is a api that is used : https://spoonacular.com/

## App Architecture

I choosed the clean architecture for this app. because architecture should be
scalable i separated layers so i developed mulit-module clean app.
there is modules in application :
* App Module : application entry point which has Application class and has
core and presentation module on dependencies.
* Presentation Module :  represent presentaion layer include of android ui
component (fragments, views, etc) and ViewModels. it has domain and core modules
on dependencies.
* Domain Module : represent domain layer of clean architecture and center of
application logic. it is not an android library module and it only has core module
on dependencies. this module contains all data layer abstractions and usecases
that presentation layer use.
*Data Module : as clean architecture data layer it has all domain abstractions
implementation. this module contains repository pattern and data sources. also
it depends on domain and core module.
*Core module :  shares some util classes and libraries with other modules.

## App Patterns
In this application i used MVVM as architectural pattern. also repository pattern
in data layer. communication between views and viewModels are observe pattern.

## App Implementation
Application is single activity and i implemented 2 fragments for list and details.
i checked api and i've found a collection list and details. so i implemented a search
and a detail page. application will cache the details but because i ran out
of time i didnt use work manager to handle connectivity constraints, offline-first list,
shared animation between fragments, data layer tests, etc

## Dependencies and libraries
This dependencies could be found in versions.gradle file.

* Kotlin programming languge, core and ktx
* Google lifecycle extensions as
* Kotlin coroutines for Threading
* Dagger for DI
* Navigation component
* Retrofit, OkHttp, okHttpLoggingInterceptorVerion, gsonConverterVersion and Gson for networking.
* Room as local database ORM
* Junit, mockitoKotlin, mockitoInline, coreTesting for unit testing.

## Unit Testing
Application has unit test for viewModels, usecases, repositories and data souces.
There is a gradle task to run all modules unit tests.
```
./gradlew testAll
```
## To Reviewer
I ran out of time and i know i can do it more better specially in UI and i know that it is better
to have a good and filled detail page with api call. i know you understand me.
also you will see kind of mappers hell in project. i know that i could rid of it but its important
to have model for each layer and i should and must show myself with these things.
at the end.. i know that documentation IS for public api's not any methods.
Thanks.





package com.vahabghadiri.domain.feature.detail.usecase

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.vahabghadiri.core.utils.testCase
import com.vahabghadiri.domain.feature.detail.repository.RecipeDetailRepository
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
internal class GetRecipeDetailUseCaseTest {

    private lateinit var useCase: GetRecipeDetailUseCase
    private val recipeDetailRepository: RecipeDetailRepository = mock()

    @Before
    fun setup() {
        useCase = GetRecipeDetailUseCase(recipeDetailRepository)
    }

    @Test
    fun `should call for data layer on execute`() = testCase {
        val fakeId = "abcd"
        val param = RecipeDetailParam(fakeId)

        whenever(useCase) {
            runBlocking {
                execute(param)
            }
        }
        then {
            runBlocking {
                verify(recipeDetailRepository, times(1)).getRecipeDetail(
                    param.id
                )
            }
        }
    }
}
package com.vahabghadiri.data

object NetworkConstants {

    const val API_KEY = "d0a40fbddcc44942bc4f82073b16087b"
    const val BASE_URL = "https://api.spoonacular.com/"

    const val DEFAULT_PAGE_COUNT = 10
}
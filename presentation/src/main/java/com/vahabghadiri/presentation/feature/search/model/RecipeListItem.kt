package com.vahabghadiri.presentation.feature.search.model

data class RecipeListItem(
    val id: String,
    val title: String,
    val image: String
)
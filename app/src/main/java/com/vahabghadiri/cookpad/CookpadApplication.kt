package com.vahabghadiri.cookpad

import com.vahabghadiri.cookpad.di.AppComponent
import com.vahabghadiri.cookpad.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

class CookpadApplication : DaggerApplication() {

    private lateinit var appComponent: AppComponent

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val injector = DaggerAppComponent.builder()
            .applicationContext(this)
            .build()
        appComponent = injector
        return injector
    }
}
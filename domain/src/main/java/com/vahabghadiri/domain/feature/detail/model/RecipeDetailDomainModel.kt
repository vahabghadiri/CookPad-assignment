package com.vahabghadiri.domain.feature.detail.model

/**
 * domain model for recipe details
 * this data class will be used in domain layer
 * and it will be result of data layer model mapping (dto's)
 */
data class RecipeDetailDomainModel(
    val id: String,
    val title: String,
    val image: String,
    val instruction: String
)
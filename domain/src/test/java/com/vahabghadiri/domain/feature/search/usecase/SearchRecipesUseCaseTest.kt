package com.vahabghadiri.domain.feature.search.usecase

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.vahabghadiri.core.utils.testCase
import com.vahabghadiri.domain.feature.search.repository.RecipeSearchRepository
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
internal class SearchRecipesUseCaseTest {

    private lateinit var useCase: SearchRecipesUseCase
    private val recipeSearchRepository: RecipeSearchRepository = mock()

    @Before
    fun setup() {
        useCase = SearchRecipesUseCase(recipeSearchRepository)
    }

    @Test
    fun `should call for data layer on execute`() = testCase {
        val fakeQueryString = "abcd"
        val fakeOffset = 0
        val param = SearchRecipeParam(fakeQueryString)

        whenever(useCase) {
            runBlocking {
                execute(param)
            }
        }
        then {
            runBlocking {
                verify(recipeSearchRepository, times(1)).searchRecipes(
                    param.searchQuery,
                    fakeOffset
                )
            }
        }
    }
}
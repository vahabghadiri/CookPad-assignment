package com.vahabghadiri.presentation.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.vahabghadiri.presentation.di.DaggerViewModelFactory
import com.vahabghadiri.presentation.di.ViewModelKey
import com.vahabghadiri.presentation.feature.detail.DetailViewModel
import com.vahabghadiri.presentation.feature.search.SearchViewModel
import com.vahabghadiri.presentation.feature.splash.SplashViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import javax.inject.Singleton

/**
 * di module to provide view models
 * and view model factory itseld
 */
@Module
abstract class ViewModelModule {

    @Binds
    @Singleton
    internal abstract fun bindViewModelFactory(
        factory: DaggerViewModelFactory
    ): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    abstract fun bindSplashViewModel(
        splashViewModel: SplashViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SearchViewModel::class)
    abstract fun bindSearchViewModel(
        searchViewModel: SearchViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DetailViewModel::class)
    abstract fun bindDetailViewModel(
        detailViewModel: DetailViewModel
    ): ViewModel
}
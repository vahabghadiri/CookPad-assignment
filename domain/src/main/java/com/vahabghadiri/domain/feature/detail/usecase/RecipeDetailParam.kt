package com.vahabghadiri.domain.feature.detail.usecase

import com.vahabghadiri.domain.model.usecase.UseCaseParam

/**
 * UseCase param for using recipe detail UseCase
 */
data class RecipeDetailParam(
    val id: String
) : UseCaseParam()
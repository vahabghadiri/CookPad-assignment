package com.vahabghadiri.presentation.feature.detail

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.vahabghadiri.core.utils.GlobalDispatcher
import com.vahabghadiri.domain.feature.detail.model.RecipeDetailDomainModel
import com.vahabghadiri.domain.feature.detail.usecase.GetRecipeDetailUseCase
import com.vahabghadiri.domain.feature.detail.usecase.RecipeDetailParam
import com.vahabghadiri.domain.model.Result
import com.vahabghadiri.presentation.base.BaseViewModel
import com.vahabghadiri.presentation.feature.detail.model.RecipeDetailViewItem
import com.vahabghadiri.presentation.utils.SingleLiveEvent
import kotlinx.coroutines.launch
import javax.inject.Inject

class DetailViewModel @Inject constructor(
    private val getRecipeDetailUseCase: GetRecipeDetailUseCase,
    private val globalDispatcher: GlobalDispatcher
) : BaseViewModel() {

    private val _progressVisibilityLiveData = MutableLiveData<Boolean>()
    val progressVisibilityLiveData: LiveData<Boolean> = _progressVisibilityLiveData

    private val _failureEventLiveData = SingleLiveEvent<String?>()
    val failureEventLiveData: LiveData<String?> = _failureEventLiveData

    private val _updateViewLiveData = MutableLiveData<RecipeDetailViewItem>()
    val updateViewLiveData: LiveData<RecipeDetailViewItem> = _updateViewLiveData

    fun onDetailViewCreated(recipeId: String, savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            return
        }
        getDetails(recipeId)
    }

    private fun getDetails(recipeId: String) {
        viewModelScope.launch(globalDispatcher.main) {
            setLoadingProgress(true)
            when (
                val result = getRecipeDetailUseCase.execute(RecipeDetailParam(recipeId))
            ) {
                is Result.Success -> handleDataSucceed(result.value)
                is Result.Failure -> _failureEventLiveData.value = result.error
            }
            setLoadingProgress(false)
        }
    }

    private fun handleDataSucceed(value: RecipeDetailDomainModel) {
        val viewItem = RecipeDetailViewItem(
            value.title,
            value.image,
            value.instruction
        )
        _updateViewLiveData.value = viewItem
    }

    private fun setLoadingProgress(isVisible: Boolean) {
        _progressVisibilityLiveData.value = isVisible
    }

    fun onRetryClick(recipeId: String) {
        getDetails(recipeId)
    }
}
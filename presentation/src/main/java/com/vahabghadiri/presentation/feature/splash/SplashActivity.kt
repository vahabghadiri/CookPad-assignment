package com.vahabghadiri.presentation.feature.splash

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.vahabghadiri.presentation.R
import com.vahabghadiri.presentation.base.BaseActivity
import com.vahabghadiri.presentation.feature.main.MainActivity

/**
 * splash activity is app first view
 * and it will disappear base on it's view model logic
 */
class SplashActivity : BaseActivity() {

    private lateinit var viewModel: SplashViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        setupViewModel()
        viewModel.onSplashViewCreated()
    }

    private fun setupViewModel() {
        viewModel = ViewModelProvider(
            this,
            viewModelFactory
        )[SplashViewModel::class.java]

        viewModel.navigateToMainLiveData.observe(this) {
            navigateToMain()
        }
    }

    private fun navigateToMain() {
        if (!isFinishing) {
            startActivity(
                MainActivity.getLaunchIntent(this).apply {
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                }
            )
            finish()
        }
    }
}
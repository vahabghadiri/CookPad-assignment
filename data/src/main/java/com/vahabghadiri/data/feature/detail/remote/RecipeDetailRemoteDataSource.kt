package com.vahabghadiri.data.feature.detail.remote

import com.vahabghadiri.core.utils.GlobalDispatcher
import com.vahabghadiri.data.feature.detail.model.RecipeDetailDataModel
import com.vahabghadiri.data.feature.detail.toDataModel
import com.vahabghadiri.data.utils.ApiResult
import com.vahabghadiri.data.utils.callAwait
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RecipeDetailRemoteDataSource @Inject constructor(
    private val recipeDetailApiService: RecipeDetailApiService,
    private val globalDispatcher: GlobalDispatcher
) {

    /**
     * It is better to have thread select here because each data provider knows
     * better that which thread is suitable for this data
     * and also it prevent issues in other layers.
     */
    suspend fun getRecipeDetail(
        recipeId: String
    ): ApiResult<RecipeDetailDataModel> {
        return withContext(globalDispatcher.io) {
            recipeDetailApiService.getRecipeDetails(recipeId).callAwait {
                it.toDataModel()
            }
        }
    }
}
package com.vahabghadiri.presentation.di.module

import com.vahabghadiri.presentation.feature.splash.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * di module for injection in activities so they will contribute for
 * android injection.
 */
@Module
abstract class ActivityModule {

    @ContributesAndroidInjector
    abstract fun splashActivity(): SplashActivity
}
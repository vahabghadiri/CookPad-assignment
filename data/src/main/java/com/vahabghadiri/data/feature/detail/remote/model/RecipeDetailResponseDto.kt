package com.vahabghadiri.data.feature.detail.remote.model

import com.google.gson.annotations.SerializedName

data class RecipeDetailResponseDto(
    @SerializedName("id") val id: String,
    @SerializedName("title") val title: String,
    @SerializedName("image") val image: String,
    @SerializedName("instructions") val instructions: String
)
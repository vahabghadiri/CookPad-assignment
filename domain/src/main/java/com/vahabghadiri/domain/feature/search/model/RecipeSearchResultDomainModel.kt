package com.vahabghadiri.domain.feature.search.model

/**
 * domain model for recipe search
 * this data class will be used in domain layer
 * and it will be result of data layer model mapping (dto's)
 */
data class RecipeSearchResultDomainModel(
    val results: List<RecipeDomainModel>,
    val offset: Int,
    val number: Int,
    val totalResults: Int,
)

data class RecipeDomainModel(
    val id: String,
    val title: String,
    val image: String
)
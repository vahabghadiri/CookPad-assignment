package com.vahabghadiri.presentation.utils.extension

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

/**
 * fragment extension to create view model
 * in a more readable and less painful way
 */
inline fun <reified T : ViewModel> Fragment.createViewModel(
    factory: ViewModelProvider.Factory,
    body: T.() -> Unit = {}
): T {
    val viewModel = ViewModelProvider(viewModelStore, factory)[T::class.java]
    viewModel.body()
    return viewModel
}
package com.vahabghadiri.presentation.feature.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.vahabghadiri.core.utils.GlobalDispatcher
import com.vahabghadiri.domain.feature.search.model.RecipeSearchResultDomainModel
import com.vahabghadiri.domain.feature.search.usecase.SearchRecipeParam
import com.vahabghadiri.domain.feature.search.usecase.SearchRecipesUseCase
import com.vahabghadiri.domain.model.Result
import com.vahabghadiri.presentation.base.BaseViewModel
import com.vahabghadiri.presentation.feature.search.model.RecipeListItem
import com.vahabghadiri.presentation.utils.SingleLiveEvent
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

typealias RecipeId = String

class SearchViewModel @Inject constructor(
    private val searchRecipesUseCase: SearchRecipesUseCase,
    private val globalDispatcher: GlobalDispatcher
) : BaseViewModel() {

    /**
     * will update page recycler view
     * with new list and it uses DiffUtil class
     */
    private val _updateListLiveData = MutableLiveData<List<RecipeListItem>>()
    val updateListLiveData: LiveData<List<RecipeListItem>> = _updateListLiveData

    private val _navigateToDetailsLiveData = SingleLiveEvent<RecipeId>()
    val navigateToDetailsLiveData: LiveData<RecipeId> = _navigateToDetailsLiveData

    private val _progressVisibilityLiveData = MutableLiveData<Pair<Boolean, Boolean>>()
    val progressVisibilityLiveData: LiveData<Pair<Boolean, Boolean>> = _progressVisibilityLiveData

    private val _emptyViewVisibilityLiveData = MutableLiveData<Boolean>()
    val emptyViewVisibilityLiveData: LiveData<Boolean> = _emptyViewVisibilityLiveData

    private val _failureEventLiveData = SingleLiveEvent<String?>()
    val failureEventLiveData: LiveData<String?> = _failureEventLiveData

    /**
     * this job is responsible for search input debounce
     * so user can enter a full word before api calls
     * @see onUserChangedSearchString
     */
    private var searchDelayJob: Job? = null

    /**
     * this job is api call job so viewModel can
     * decide to have multiple api call or cancel the last one
     * @see searchQuery
     */
    private var searchJob: Job? = null

    /**
     * represent the latest search query so
     * view model can use that to Retry
     * @see searchLatestQuery
     */
    private var latestSearchQuery: String? = null

    /**
     * to handle pagination view model store latest items
     * and will clear it on certain situations
     */
    private var latestItems: MutableList<RecipeListItem> = mutableListOf()

    /**
     * will be called on search edit text change
     * and checks if query is OK, then uses debounce and
     * start to search user query string.
     * @see isSearchQueryValid
     * @see startSearchDebounce
     */
    fun onUserChangedSearchString(query: String) {
        if (latestSearchQuery == query) {
            return
        }
        resetSearch()
        searchDelayJob?.cancel()

        if (query.isEmpty()) {
            searchJob?.cancel()
            _updateListLiveData.value = emptyList()
            return
        }
        if (!isSearchQueryValid(query)) {
            return
        }
        searchDelayJob = startSearchDebounce(query)
    }

    private fun isSearchQueryValid(query: String): Boolean = query.length >= SEARCH_MIN_CHARACTER

    /**
     * debounce for search query input
     */
    private fun startSearchDebounce(query: String): Job =
        viewModelScope.launch(globalDispatcher.io) {
            delay(SEARCH_DEBOUNCE_THRESHOLD_MS)
            withContext(globalDispatcher.main) {
                searchQuery(query)
            }
        }

    private fun searchQuery(queryString: String) {
        searchJob?.cancel()

        if (searchRecipesUseCase.isReachedTheLastPage()) {
            return
        }
        searchJob = viewModelScope.launch(globalDispatcher.main) {
            latestSearchQuery = queryString
            setPageLoading(true)
            dismissErrorView()

            val param = SearchRecipeParam(queryString)
            when (
                val result = searchRecipesUseCase.execute(param)
            ) {
                is Result.Success -> handleDataSucceed(result.value)
                is Result.Failure -> _failureEventLiveData.value = result.error
            }
            setPageLoading(false)
        }
    }

    private fun handleDataSucceed(result: RecipeSearchResultDomainModel) {
        val newItems = mapToPageItem(result)
        latestItems.addAll(newItems)
        _updateListLiveData.value = latestItems
        _emptyViewVisibilityLiveData.value = latestItems.isEmpty()
    }

    private fun mapToPageItem(result: RecipeSearchResultDomainModel): List<RecipeListItem> =
        result.results.map { it.toListItem() }

    /**
     * will be fired when page gets to end
     * this method comes directly through list scroll callbacks.
     */
    fun onPageLoadMoreItem() {
        searchLatestQuery()
    }

    private fun searchLatestQuery() {
        if (latestSearchQuery.isNullOrBlank()) {
            return
        }
        searchQuery(requireNotNull(latestSearchQuery))
    }

    private fun setPageLoading(isLoading: Boolean) {
        val shouldShowCenterProgress = latestItems.isEmpty()
        _progressVisibilityLiveData.value = isLoading to shouldShowCenterProgress
    }

    private fun dismissErrorView() {
        _failureEventLiveData.value = null
    }

    /**
     * when certain page getting data failed, user
     * can retry it and this method would be called.
     */
    fun onRetryClick() {
        searchLatestQuery()
    }

    fun onSearchResultItemClicked(item: RecipeListItem) {
        _navigateToDetailsLiveData.value = item.id
    }

    /**
     * will reset paging params and stored items
     * from previous data.
     * this method will be called when user start
     * to search new query
     */
    private fun resetSearch() {
        setPageLoading(false)
        dismissErrorView()
        latestItems.clear()
        _emptyViewVisibilityLiveData.value = false
        searchRecipesUseCase.refreshToFirstPage()
    }

    private companion object {

        const val SEARCH_DEBOUNCE_THRESHOLD_MS = 500L
        const val SEARCH_MIN_CHARACTER = 3
    }
}
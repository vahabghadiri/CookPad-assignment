package com.vahabghadiri.domain.feature.search.usecase

import com.vahabghadiri.domain.model.usecase.UseCaseParam

/**
 * UseCase param for using search UseCase
 */
data class SearchRecipeParam(
    val searchQuery: String
) : UseCaseParam()
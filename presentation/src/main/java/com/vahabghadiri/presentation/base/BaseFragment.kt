package com.vahabghadiri.presentation.base

import androidx.lifecycle.ViewModelProvider
import dagger.android.support.DaggerFragment
import javax.inject.Inject

/**
 * base class for fragments to handle
 * view model factory provide
 */
open class BaseFragment : DaggerFragment() {

    /**
     * will provide factory for viewModel creation
     */
    @Inject
    protected lateinit var viewModelFactory: ViewModelProvider.Factory
}
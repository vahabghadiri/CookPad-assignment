package com.vahabghadiri.data.feature.search.remote

import com.vahabghadiri.core.utils.GlobalDispatcher
import com.vahabghadiri.data.NetworkConstants.DEFAULT_PAGE_COUNT
import com.vahabghadiri.data.feature.search.model.RecipeSearchDataModel
import com.vahabghadiri.data.feature.search.toDataModel
import com.vahabghadiri.data.utils.ApiResult
import com.vahabghadiri.data.utils.callAwait
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RecipeSearchRemoteDataSource @Inject constructor(
    private val recipeSearchApiService: RecipeSearchApiService,
    private val globalDispatcher: GlobalDispatcher
) {

    /**
     * It is better to have thread select here because each data provider knows
     * better that which thread is suitable for this data
     * and also it prevent issues in other layers.
     */
    suspend fun searchRecipes(
        searchQuery: String,
        offset: Int
    ): ApiResult<RecipeSearchDataModel> {
        return withContext(globalDispatcher.io) {
            recipeSearchApiService.getSearchResult(
                searchQuery,
                DEFAULT_PAGE_COUNT,
                offset
            ).callAwait {
                it.toDataModel()
            }
        }
    }
}
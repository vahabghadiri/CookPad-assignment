package com.vahabghadiri.data.feature.detail.local.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "recipe_detail")
data class RecipeDetailLocalEntity(

    @PrimaryKey
    val id: String,
    val title: String,
    val image: String,
    val instructions: String
)
package com.vahabghadiri.cookpad.di

import android.content.Context
import com.vahabghadiri.cookpad.CookpadApplication
import com.vahabghadiri.core.di.CoreModule
import com.vahabghadiri.data.di.DatabaseModule
import com.vahabghadiri.data.di.NetworkModule
import com.vahabghadiri.data.di.RemoteApiModule
import com.vahabghadiri.data.di.RepositoryModule
import com.vahabghadiri.presentation.di.module.ActivityModule
import com.vahabghadiri.presentation.di.module.FragmentModule
import com.vahabghadiri.presentation.di.module.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        CoreModule::class,
        NetworkModule::class,
        RepositoryModule::class,
        RemoteApiModule::class,
        DatabaseModule::class,
        ActivityModule::class,
        FragmentModule::class,
        ViewModelModule::class
    ]
)
interface AppComponent : AndroidInjector<CookpadApplication> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun applicationContext(applicationContext: Context): Builder
        fun build(): AppComponent
    }
}
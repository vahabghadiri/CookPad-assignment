package com.vahabghadiri.presentation.feature.search.adapter

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.vahabghadiri.presentation.R
import com.vahabghadiri.presentation.feature.search.model.RecipeListItem
import com.vahabghadiri.presentation.utils.ImageLoader

/**
 * view holder for recipe search result
 * @see SearchListRvAdapter
 */
class SearchResultItemViewHolder(
    itemView: View,
    adapterCallback: AdapterCallback
) : RecyclerView.ViewHolder(itemView) {

    private val tvTitle = itemView.findViewById<TextView>(R.id.tvTitle)
    private val ivImage = itemView.findViewById<ImageView>(R.id.ivImage)

    init {
        itemView.setOnClickListener {
            if (adapterPosition != RecyclerView.NO_POSITION) {
                adapterCallback.onItemClick(adapterPosition)
            }
        }
    }

    fun bind(item: RecipeListItem) {
        tvTitle.text = item.title
        ImageLoader.loadThumbnail(ivImage, item.image)
    }
}
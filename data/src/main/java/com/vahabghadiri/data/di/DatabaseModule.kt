package com.vahabghadiri.data.di

import android.content.Context
import androidx.room.Room
import com.vahabghadiri.data.db.AppDatabase
import com.vahabghadiri.data.feature.detail.local.RecipeDetailDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * will provide Database and it's DAO objects for data layer
 */
@Module
class DatabaseModule {

    @Singleton
    @Provides
    fun provideDatabase(context: Context): AppDatabase {
        return Room.databaseBuilder(
            context,
            AppDatabase::class.java,
            "cookpad-app-db"
        ).fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideDetailDao(appDatabase: AppDatabase): RecipeDetailDao {
        return appDatabase.recipeDetailDao()
    }
}
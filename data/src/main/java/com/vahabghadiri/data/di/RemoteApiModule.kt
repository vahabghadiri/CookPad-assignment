package com.vahabghadiri.data.di

import com.vahabghadiri.data.feature.detail.remote.RecipeDetailApiService
import com.vahabghadiri.data.feature.search.remote.RecipeSearchApiService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * will provide network api services
 * which will be used in data module and remote data sources.
 */
@Module
open class RemoteApiModule {

    @Singleton
    @Provides
    open fun provideSearchService(retrofit: Retrofit): RecipeSearchApiService =
        retrofit.create(RecipeSearchApiService::class.java)

    @Singleton
    @Provides
    open fun provideDetailService(retrofit: Retrofit): RecipeDetailApiService =
        retrofit.create(RecipeDetailApiService::class.java)
}
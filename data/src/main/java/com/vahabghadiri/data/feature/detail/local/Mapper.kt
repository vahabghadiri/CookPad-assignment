package com.vahabghadiri.data.feature.detail.local

import com.vahabghadiri.data.feature.detail.local.model.RecipeDetailLocalEntity
import com.vahabghadiri.data.feature.detail.model.RecipeDetailDataModel

fun RecipeDetailDataModel.toLocalEntity() = RecipeDetailLocalEntity(
    id, title, image, instructions
)

fun RecipeDetailLocalEntity.toDataModel() = RecipeDetailDataModel(
    id, title, image, instructions
)
